# Base Magnolia Java webapp

created using the following Maven command:

    mvn archetype:generate \
        -DarchetypeGroupId=info.magnolia.maven.archetypes \
        -DarchetypeArtifactId=magnolia-dx-core-custom-cloud-archetype \
        -DarchetypeVersion=1.2

using these values:

    groupId: info.magnolia.next
    artifactId: demo
    version: 1.0-SNAPSHOT
    package: info.magnolia.next
    magnolia-bundle-version: 6.2.15
    project-name: demo
    shortname: next

Please refer to https://maven.apache.org/archetype/maven-archetype-plugin/archetype-repository.html for configuring the archetype resolution and add this repository to your active profile in your ~/.m2/settings.xml:

    <repository>
        <id>archetype</id><!-- id expected by maven-archetype-plugin to avoid fetching from everywhere -->
        <url>nexus.magnolia-cms.com/content/repositories/magnolia.public.releases</url>
        <releases>
            <enabled>true</enabled>
            <checksumPolicy>fail</checksumPolicy>
        </releases>
        <snapshots>
            <enabled>true</enabled>
            <checksumPolicy>warn</checksumPolicy>
        </snapshots>
    </repository>